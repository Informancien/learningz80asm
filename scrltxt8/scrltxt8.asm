; Scroll texte avec déplacement par octets

ORG #4000

start:
	ld a, 2		; Argument passé à la routine
	call #BC0E	; Passage mode 2

	ld hl, string		; Charger l'adresse de la chaîne

crtc_line:

	call #BD19	; Attente du VBLANK

	push hl		; Sauvegarde de HL

	ld b, 8		; Compteur de scanlines
	ld hl, #c001	; Seconde adresse de la scanline

.scanline:

	push bc		; Sauvegarde de B (16 bits seulement, donc C aussi)
	ld bc, 79	; Nombre d'octets par ligne, utilisé par LDIR

	push hl		; Sauvegarde de HL
	ld d, h
	ld e, l		; Copie de HL dans DE
	dec e		; Décrementation, après de l'octet précédent
	
	ldir		; Copie de bloc, la triche!

	pop hl		; Restauration de HL
	ld a, h		; Copie de H dans A
	add a, 8	; Addition de 8 pour passer à la scanline suivante
	ld h, a		; Recopie de A dans H

	pop bc		; Restauration de B (C avec, que ddu 16 bits)&
	djnz .scanline	; Scanline suivante

text:

	ld hl, 80*256+1		; HL contient la colonne et ligne du curseur
	call #BB75		; Positionnement du curseur à HL

	pop hl		; Restauration de HL, l'adresse du prochain char

	ld a, (hl)		; Charger dans A le char à l'adresse HL

	or a
	jr nz, .draw		; Boucler jusqu'à l'octet nul

	ld hl, string 
	ld a, (hl)		; Charger dans A le char à l'adresse HL

.draw:
	call #BB5A		; Ecrire un char et déplacer le curseur

	inc hl			; Char suivant

	jr crtc_line ; Ligne CRTC finie, recommencer

string:

DEFM 'CPC rulez da world! ',0

end:

SAVE 'scrltxt8.bin',start,end-start,DSK,'scrltxt8.dsk'

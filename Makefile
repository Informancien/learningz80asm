# Makefile de génération des .dsk à partir des .asm 
# Passer le but `run` en argument après le .dsk le lancera dans l'émulateur

# Assembleur
ASM := rasm.exe
# Options de l'assembleur
ASMFLAGS := -eo
# Emulateur
EMU := cpcec-plus

# Le fichier envvars contient les variables d'environnement comme PATH
-include envvars

# Liste des .dsk à générer
DSKFILES := $(foreach asm,$(wildcard */*.asm),$(subst .asm,.dsk,$(asm)))
DSK = $(firstword $(MAKECMDGOALS))
SHELL = /bin/bash

.PHONY: all run clean mrproper

all: $(DSKFILES) # Générer tous les .dsk

%.dsk: %.asm # Assemblage, lance les Makefiles accompagnant les .asm 
	@[[ -f '$(<D)/Makefile' ]] && $(MAKE) -C $(<D) || true
	cd $(<D) && $(ASM) $(ASMFLAGS) $(<F)


run: # `make <dsk> run` lance le .dsk <dsk> avec l'émulateur $(EMU)
	@[[ -f $(DSK) ]] || ( echo "Invalid .dsk file '$(DSK)'"; false )
	@`whereis $(EMU) | cut -d ' ' -f 2` $(DSK)

clean: 
	rm -f */*.dat

mrproper: clean
	rm -f */*.dsk

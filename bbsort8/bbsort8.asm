; Tri à bulle sur 256 octets

ORG #4000	; Adresse d'implémentation
LENGHT EQU 255	; Longueur du tableau (octets)

start
	ld e, LENGHT-1	; Charger longueur dans registre e

.pass
	ld b, e		; b est la longueur de passe, initialement e-1
	ld ix, tbl	; Registre d'index, adresse de la table
	ld c, %0	; Drapeau à 1 si commutation dans l'itération

.switch
	ld a, (ix)	; Charger l'octet courant dans l'accumulateur
	cp (ix+1)	; Comparaison de a et le contenu à l'adresse ix+1

	jr c, .skip	; Si la comparaison donne déborde, on saute
	jr z, .skip	; Si la comparaison donne zéro, on saute aussi
	
			; Permutation des valeurs
	ld d, (ix+1)	; Pas d'instruction mémoire/mémoire, donc copie dans d
	ld (ix), d	; Le contenu de d va à ix
	ld (ix+1), a	; Le contenu de a va à ix+1

	ld c, %1	; Drapeau de commutation levé

.skip
	inc ix		; Adresse suivante
	djnz .switch	; Si b (longueur du tableau) est non-nul, réitération

	dec e		; Décrementation de longueur de passe

	ld a, c		; Charger l'accumulateur avec le drapeau de commutation
	cp %1		; Comparaison de a à %1
	jr z, .pass	; Occurence d'une commutation, nouvelle passe

	ret		; Fini

tbl
	incbin 'rnddat.dat' ; Fichier créé par le Makefile

end

SAVE 'bbsort8.bin',start,end-start,DSK,'bbsort8.dsk'

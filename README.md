# Apprentissage de l'assembleur Z80

Ce dépôt contient un ensemble de programmes écrits dans le cadre d'un
apprentissage de l'assembleur Z80 sur Amstrad CPC.

L'assembleur utilisé est [rasm](https://github.com/EdouardBERGE/rasm) et
l'émulateur recommandé [cpcec-plus](https://gitlab.com/norecess464/cpcec-plus). 

## Programmes

Chaque dossier contient un programme écrit en assembleur Z80:
* `bbsort8/` Tri à bulle de 256 octets.
* `string/` Affichage d'une chaîne par appels systèmes;
* `scrltxt8/` Scroll texte avec déplacement par octets;
* `scrlnsys/` Scroll texte sans appels systèmes dans la main loop.

### Assemblage

Le `Makefile` à la racine du dépôt génére les images disquettes à partir des
`.asm`. Chaque fichier `.dsk` est créé dans le même dossier que sa source. 
Certaines sources sont accompagnées de leurs propres `Makefile`, ils seront
automatiquement lancés par celui du dépôt.

`make` peut être invoqué avec les buts suivants:
* `all` Cible par défaut, assemble les programmes avec `rasm.exe`;
* `<program>.dsk` Assemble la source `<program>.asm` dans une image disquette,
  le **dossier contenant le programme doit être précisé**;
* `<dsk> run` Assemble et lance la diquette `<dsk>` dans l'émulateur
  `cpcec-plus`;
* `clean` Supprime les fichiers intermédiraires comme les `.dat`;
* `mrproper` Supprime tout les fichiers générés.

Les binaires `rasm.exe` et `cpcec-plus` doivent être présents dans l'un des
dossiers de la variable d'environnement `PATH`. L'environnement peut être
défini dans un fichier nommé `envvars` à la racine du dépôt, il sera inclu par
le `Makefile`.

## Licence

Tout les programmes de ce dépôt sont sous licence MIT (aussi connue sous le nom
Expat).

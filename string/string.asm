; Affichage d'une chaîne de caractères

ORG #4000

start:
	ld a, 2		; Argument passé à la routine
	call #BC0E	; Passage mode 2

	ld hl, 10*256+10	; HL contient la colonne et ligne du curseur
	call #BB75		; Positionnement du curseur à HL

	ld hl, string		; Charger l'adresse de la chaîne

.display:

	ld a, (hl)		; Charger dans A le char à l'adresse HL

	or a
	jr z, .exit		; Boucler jusqu'à l'octet nul

	call #BB5A		; Ecrire un char et déplacer le curseur
	inc hl			; Adresse suivante

	jr .display

.exit:

	call #BB18		; Attendre la prochaine saisie clavier
	ret

string:

DEFM 'CPC rulez da world',0

end:

SAVE 'string.bin',start,end-start,DSK,'string.dsk'

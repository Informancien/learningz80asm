; Scroll texte avec déplacement par octets sans appels systèmes

ORG #4000

DRAW_ADDR EQU #c000+79

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Main loop
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

start

	call get_bitmap	; Copie la bitmap des chars depuis la ROM vers la RAM

	ld a, 2		; Argument passé à la routine
	call #BC0E	; Passage mode 2

	ld hl, string		; Charger l'adresse de la chaîne

	di

crtc_line

  	ld b, #f5

.waitvbl

  	in a,(c)		
  	rra
  	jr nc, .waitvbl

	;ld bc,#7f00+0+16	; PENR
	;out (c),c		; Sélection du border
	;ld bc,#7f00+64+9	; INKR
	;out (c),c		; Jaune

	push hl		; Sauvegarde de HL

	ld b, 8		; Compteur de scanlines
	ld hl, #c001	; Seconde adresse de la scanline

.scanline

	push bc		; Sauvegarde de B (16 bits seulement, donc C aussi)
	ld bc, 79	; Nombre d'octets par ligne, utilisé par LDIR

	push hl		; Sauvegarde de HL
	ld d, h
	ld e, l		; Copie de HL dans DE
	dec e		; Décrementation, après de l'octet précédent
	
	ldir		; Copie de bloc, la triche!

	pop hl		; Restauration de HL
	ld a, h		; Copie de H dans A
	add a, 8	; Addition de 8 pour passer à la scanline suivante
	ld h, a		; Recopie de A dans H

	pop bc		; Restauration de B (C avec, que ddu 16 bits)
	djnz .scanline	; Scanline suivante

text

	;ld hl, 80*256+1		; HL contient la colonne et ligne du curseur
	;call #BB75		; Positionnement du curseur à HL

	pop hl		; Restauration de HL, l'adresse du prochain char

	ld a, (hl)		; Charger dans A le char à l'adresse HL

	cp #FF

	jr nz, .draw		; Boucler jusqu'à l'octet nul

	ld hl, string 
	ld a, (hl)		; Charger dans A le char à l'adresse HL

.draw

	ld e, a
	ld d, 0

	REPEAT 3

	sla e		; chaque bitmap de char fait 8B donc l'adresse est 8*A
			; Décalage fois 2, répété 3 fois donne 8 fois
	rl d

	REND

	push hl		; Sauvegarde de HL

	ld hl, bitmap		; Calcul de l'adresse de la bitmap dans HL
	add hl,	de

	ld de, DRAW_ADDR	; DE est l'adresse en mémoire vidéo
	ld b, 8		; B est le nombre de lignes à dessiné, utilisé par DJNZ

.loop
	
	ld a, (hl)	; Chargement de la bitmap du char
	ld (de), a	; Ecriture de la bitmap à l'adresse de dessin

	push hl		; Sauvegarde de HL, adresse de la bitmap du char

	ex hl, de	; Adresse de dessin dans l'accumulateur 16b
	ld de, #800	; Valeur d'incrément d'une ligne

	add hl, de	; DE incrémenté de #800 (adresse mémoire vidéo)
	ex hl, de

	pop hl		; Restauration de HL
	inc hl		; Passage au prochain octet de la bitmap
	
	djnz .loop

	pop hl			; Restauration de HL
	inc hl			; Char suivant

	;ld bc,#7f00+0+16
	;out (c),c
	;ld bc,#7f00+64+21	; Bleu
	;out (c),c

	jr crtc_line ; Ligne CRTC finie, recommencer

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Routines
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

FIRST_CHAR EQU #20
LAST_CHAR EQU #5A

get_bitmap

	call #B906			; Passer en ROM haute

	ld a, FIRST_CHAR		; Premier char pour l'appel système
	ld b, LAST_CHAR-FIRST_CHAR+1	; Compteur pour DJNZ
	ld de, bitmap			; Adresse de copie pour LDIR

.copy_char

	push bc			; Sauvegarde de B (16 bits seulement)

	push af			; Sauvegarde de A
	call #BBA5		; Adresse du bitmap dans HL

	ld bc, 8		; Nombres de copies par LDIR
	ldir 			; Copie

	pop af			; Restauration de A
	inc a			; Pointe vers prochain char

	pop bc			; Restauration de B pour DJNZ
	djnz .copy_char

	call #B909			; Sortir de la ROM haute

	ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Data
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

string

CHARSET ' !"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ',0
DEFM 'CPC RULEZ DA WORLD! ',#FF
CHARSET		; Reset du CHARSET

bitmap

DEFS (LAST_CHAR-FIRST_CHAR+1)*8,0

end

SAVE 'scrlnsys.bin',start,end-start,DSK,'scrlnsys.dsk'
